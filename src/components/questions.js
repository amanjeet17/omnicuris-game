import React, { Component } from 'react';
import '../App.css';
import $ from 'jquery';
import Treasure1 from '../Images/newtreasure.gif';
import Empty from '../Images/empty-treasure.gif';
import Coin from '../Images/coin1.gif';
import WinHeaderCoin from '../Images/coin11.gif';
import {Animated} from "react-animated-css";
const questions =[
    {
      "QNum": 1,
      "question":"Which statement is true regarding ST segment elevation?",
      "options":["Significant ST segment elevation in acute ischemia is defined as J point elevation of more than 1 mm in 2 contiguous leads in limb leads or in precordial leads.",
                  "Significant ST segment elevation in acute ischemia is defined as J point elevation of more than 1 mm in 2 contiguous leads in limb leads or precordial leads V4 to V6 and more than 2 mm elevation in two contiguous leads in V2 to V3.",
                  "Concave ST segment elevation is associated with ischemia",
                  "Convex ST segment elevation is a normal variant"],
      "answer":"b"
    },
    {
      "QNum": 2,
      "question":"Which of the statement is True regarding ECG changes during acute myocardial ischemia ",
      "options":["Upsloping ST depression is a more specific sign of ischemia",
                 "Inverted U wave is a specific finding during early acute coronary syndrome",
                 "Inverted U wave is mainly seen in RCA infarction",
                 "Inverted U wave is seen late in myocardial infarction"],
      "answer":"b"
    },
    {
      "QNum": 3,
      "question":"Which among the following is not a feature of RBBB?",
      "options":["QRS duration> 0.12 sec in extremity leads and RSR’ in V1",
                 " S-wave in lead I aVL & V6",
                 "Intrinsicoid deflection <50 ms in V1",
                 "Secondary ST-T changes in V1"],
      "answer":"c"
    },
    {
      "QNum": 4,
      "question":"All of the following intraventricular conduction abnormalities can exist on ECG with QRS duration >0.12 seconds except?",
      "options":["LBBB",
                 "Bifascicular Block",
                 "Trifascicular Block",
                 " LAHB"],
      "answer":"d"
    },
    {
      "QNum": 5,
      "question":"Typical AVNRT can be characterized on the surface ECG with all of the following except?",
      "options":["Narrow QRS regular tachycardia with short RP interval.",
                 "Narrow QRS regular tachycardia with pseudo ‘S’ waves in inferior leads and R’ wave in lead V1",
                 "Narrow QRS regular tachycardia with P waves buried within the QRS complexes ",
                 "Narrow QRS irregular tachycardia"],
      "answer":"d"
    },
    {
      "QNum": 6,
      "question":"An orthodromic AVRT is characterized by all of the following features on the ECG except?",
      "options":["Near-simultaneous activation of the atria and the ventricles",
                 "Narrow QRS regular tachycardia with short RP interval.",
                 "P-wave located on the ST segment ",
                 " Narrow QRS regular tachycardia with ST depressions which disappear in sinus rhythm."],
      "answer":"a"
    },
    {
      "QNum": 7,
      "question":"All of the following are true regarding sick sinus syndrome except-",
      "options":["Concomitant AV nodal disease is observed in 50%",
                 "New AV nodal disease occurs at a rate of 2.7% per year",
                 "Associated with increased incidence of AF and stroke",
                 "Atrial based pacing decreases the incidence of AF"],
      "answer":"a"
    },
    {
      "QNum": 8,
      "question":"At which level does typical wenckebach AV block occur?",
      "options":["His bundle",
                 "AV node",
                 "Bundle branches",
                 "Myocardium"],
      "answer":"b"
    },
    {
      "QNum": 9,
      "question":"Which of the following is a feature of right ventricular enlargement?",
      "options":["R/S ratio > 1 in V1, R V1 > 7 mm, S < 2 mm , S in V6 > 7mm, R/S V5 or V6 < 1",
                 "Intrinsicoid deflection in V1 >0.04 Sec, RAD > 110?",
                 "Secondary ST T Changes in V1 / V2 ",
                 "All of the options"],
      "answer":"d"
    },
    {
      "QNum": 10,
      "question":"Which of the following features do not represent ECG changes in LV enlargement",
      "options":["R-wave voltage in aVL >13mm , R-wave voltage in lead I + S-wave voltage in lead III > 25 mm",
                 " S-wave voltage in V1 > 24 mm or R wave voltage in V5 / V6 > 26 mm",
                 "R/S ratio >1 in V1, R in V1 > 7 mm",
                 "S < 2 mm"],
      "answer":"d"
    }
]

class Page2 extends Component{
  constructor(props){
    super(props)
    this.state={
      correctanswer:'',
      header:'',
      output:'',
      question:'',
      options:[],
      coincount:100
    }

  }
   CheckAnswer = (event)=>{
    event.preventDefault();
    var radios = document.querySelectorAll('input[type="radio"]:checked')[0].value;
    console.log("event details",radios);
    var coincount = this.state.coincount + 10;
    if(this.state.correctanswer === radios){
      this.setState({
        output:'Your Answer is Absolutely Correct',
        header:'Hurray',
        coincount:coincount
      })
      const json = JSON.stringify(coincount);
      localStorage.setItem('coincount', json);
      $('.pts').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now)+10);
            }
        });
    });
      setInterval(function(){

      $('.pts').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
  }, 5500);
    }
    else{
      this.setState({
        output:'Sorry!! Thats a Wrong Answer',
        header:"OOPS"
      })
    }
    var x = document.getElementById("myModal")
    x.classList.remove("fade");
    x.classList.add("show");
  }
  componentDidMount(){
    const id=this.props.match.params.id;
    var arr = questions[id-1];
    this.setState({
      question:arr.question,
      options:arr.options,
      correctanswer:arr.answer
    })
    console.log("Did mount received array",id,arr);
    try {
      const json = localStorage.getItem('coincount');
      const coincount = JSON.parse(json);

      if (coincount) {
        this.setState(() => ({ coincount }));
      }
    } catch (e) {
      // Do nothing at all
    }
  }

  render(){
    return (
      <div>
      <div  class="flex " style={{background: "white" , position:"fixed",top:"0",zIndex:"10000",width:"100%"}}>
        <div  class="flex col-xs-12 first-row">
          <div  class="flex omni-container h-100">
            <div  class="flex row h-100 align-items-center header">
              <div  class="flex col-12 col-sm-3 col-md-4 col-lg-3 xs-t-b-10 logoNew">
                <div  class="flex logoWrapper">
                  <a  href="/" class="logo-link">
                    <img style={{  marginLeft: "15px",width: "50%"}} src="https://dev.omnicuris.com/img/omnicuris_logo.png" alt=""/>
                    <h6  class="logo_tagline">India's Online CME Platform</h6>
                  </a>
                </div>
              </div>
              <p className="pts" style={{marginRight:"-18px!important"}}>{this.state.coincount}</p>

              {this.state.header == 'Hurray' ? <img src={WinHeaderCoin} alt="Treasure" class="wheader-coin"/> : <img src={Coin} alt="Treasure"  className="coin header-coin"/>}
            </div>
          </div>
        </div>
      </div>

      <div className="container" style={{position: "relative",top: "60px"}}>
      <div className="App">
      </div>

      <div className="modal fade" id="myModal" role="dialog">
        <div className="modal-dialog "style={{position: "initial"}}>
          <div className="modal-content" style={{top: "80px"}}>
            <div className="modal-header" id={this.state.header == 'Hurray' ? "green": "red"} style={{color:"#fff",textAlign:"center"}}>
              <h2 className="modal-title">{this.state.header} </h2>
            </div>
            <div className="modal-body" id={this.state.header == 'Hurray' ? "win": "loss"} >
             {this.state.header == 'Hurray' ?
             <div>
                <p style={{color:"#af5727"}}><i>Congratulations! You  cleared the question and collected </i><b>10 Coins</b></p>
                <img src={Treasure1} alt="Treasure" className="modal-image"/>< br/>
                <a href="/page3">
                  <button type="button" className="btn-lg primaryoc" style={{width:"inherit" , backgroundColor:"#73AF55"}}>CONTINUE TO VIDEO</button>
                </a>
             </div> :
             <div>
             <p style={{color:"#af5727"}}><i>Wrong Answer! </i><b> Better Luck Next Time</b></p>
              <img src={Empty} alt="Treasure" className="modal-image" /><br />
              <a href="/page3">
                <button type="button" className="btn-lg primaryoc" style={{width:"inherit" , backgroundColor:"#D06079"}}>CONTINUE TO VIDEO</button>
              </a>
              </div>
            }


        </div>
          </div>
        </div>
      </div>

      <div className="container">
    	  <div className="row " id="ques">
    	   <br/>
            <div className='panel' >
             <form onSubmit={this.CheckAnswer}>
                <div className = "panel-body">
                      <h4 style={{textAlign: "left"}}>Question: {this.state.question}</h4>
                 </div>
                  <ul className = "list-group">
                    <li className = "list-group-item">
                    <Animated animationIn="zoomInDown" animationInDelay="1000" isVisible={true}>
                    <label>
                          <div className="checkbox">
                            <input type="radio" name="birth" value="a" required/> {this.state.options[0]}
                        </div>
                      </label>
                      </Animated>
                    </li>
                    <li className = "list-group-item">
                    <Animated animationIn="zoomInLeft" animationInDelay="1500" isVisible={true}>
                    <label>
                          <div className="checkbox">
                          <input type="radio" name="birth" value="b" required/> {this.state.options[1]}
                        </div>
                        </label>
                        </Animated>
                    </li>
                    <li className = "list-group-item">
                    <Animated animationIn="zoomInRight" animationInDelay="2000" isVisible={true}>
                    <label>
                          <div className="checkbox">
                          <input type="radio" name="birth" value="c" required/> {this.state.options[2]}
                        </div>
                        </label>
                        </Animated>
                    </li>
                    <li className = "list-group-item">
                    <Animated animationIn="zoomInUp" animationInDelay="2500" isVisible={true}>
                    <label>
                          <div className="checkbox">
                          <input type="radio" name="birth" value="d" required/> {this.state.options[3]}
                        </div>
                        </label>
                        </Animated>
                    </li>
                   </ul>
                   <div class="submit">
                   <Animated animationIn="zoomInUp" animationInDelay="3000" isVisible={true}>
                   <button type="submit" className="btn-lg primaryoc" > Submit Answer  </button>
                   </Animated>
                   </div>
                </form>
              </div>
        </div>
          </div>
      </div>


      </div>
    );
  }
}

export default Page2;
