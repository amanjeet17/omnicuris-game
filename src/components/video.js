import React, { Component } from 'react';
import $ from 'jquery'
import Coin from '../Images/coin1.gif';


class Page3 extends  Component {
  constructor(props){
    super(props)
    this.state={
      coincount:''
    }
  }

  componentDidMount(){

    try {
      const json = localStorage.getItem('coincount');
      const coincount = JSON.parse(json);

      if (coincount) {
        this.setState(() => ({ coincount }));
      }

    } catch (e) {
      // Do nothing at all
    }
  }

  render(){
    return (
      <div>
      <div  className="flex " style={{background: "white" , position:"fixed",top:"0",zIndex:"10000",width:"100%"}}>
        <div  className="flex col-xs-12 first-row">
          <div  className="flex omni-container h-100">
            <div  className="flex row h-100 align-items-center header">
              <div  className="flex col-12 col-sm-3 col-md-4 col-lg-3 xs-t-b-10 logoNew">
                <div  className="flex logoWrapper">
                  <a  href="/" className="logo-link">
                    <img style={{  marginLeft: "15px",width: "50%"}} src="https://dev.omnicuris.com/img/omnicuris_logo.png" alt=""/>
                    <h6  className="logo_tagline">India's Online CME Platform</h6>
                  </a>
                </div>
              </div>
              <p className="pts">{this.state.coincount}</p>

              <img src={Coin} alt="Treasure" className="coin header-coin"/>
            </div>
          </div>
        </div>
      </div>
      <div className="App" style={{margin:"15px 0px"}}>
      <div className=" container" >
        <iframe width="100%" height="450px" src="https://www.youtube.com/embed/A5nKrr5eXqA" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"  allowFullScreen="allowFullScreen"
        mozallowfullscreen="mozallowfullscreen"
        msallowfullscreen="msallowfullscreen"
        oallowfullscreen="oallowfullscreen"
        webkitallowfullscreen="webkitallowfullscreen"></iframe>
      </div>
          <a href="/">
          <button type="button"  className="btn-lg game">GO TO GAME</button>
          </a>
      </div>

      </div>
    );
  }
}

export default Page3;
