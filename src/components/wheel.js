import React, { Component } from 'react';
import '../App.css';
import $ from 'jquery'
import Coin from '../Images/coin1.gif';
import {Animated} from "react-animated-css";

var spinit = true;
class Wheel extends Component{
  constructor(props){
    super(props);
    this.state={
      question:'???',
      topic:'',
      coincount:100
    }
  }

   spin =()=>{
  var self = this;
  if(spinit){
    spinit=false
  var degree = 1800;
  var clicks = 0;


  clicks ++;


  var newDegree = degree*clicks;
  var extraDegree = Math.floor(Math.random() * (360 - 1 + 1)) + 1;
  var totalDegree = newDegree+extraDegree;
  console.log("totalDegree",totalDegree);
  var x = totalDegree%360;
  var ques;
  if(x>=308 && x<=344 ){
    ques = 1;
    self.setState({
      topic:"STEMI"
    });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(1)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(1)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);

  }
  else if (x>=272 && x<=307) {
      ques = 2;
      self.setState({
        topic:"AMI"
      });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(2)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(2)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  else if (x>=236 && x<=271) {
      ques = 3;
      self.setState({
        topic:"RBBB"
      });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(3)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(3)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  else if (x>=200 && x<=235) {
    ques = 4;
    self.setState({
      topic:"LAHB"
    });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(4)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(4)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  else if (x>=164 && x<=199) {
    ques = 5 ;
    self.setState({
      topic:"AVNRT"
    });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(5)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(5)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  else if (x>=128 && x<=163) {
    ques = 6;
    self.setState({
      topic:"AVRT"
    });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(6)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(6)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  else if (x>=92 && x<=127) {
    ques = 7;
    self.setState({
      topic:"SSS"
    });
    let x = true;
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(7)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(7)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  else if (x>=56 && x<=91) {
    ques = 8;
    self.setState({
      topic:"AV BLOCK"
    });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(8)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(8)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  else if (x>=20 && x<=55) {
    ques = 9;
    self.setState({
      topic:"RVE"
    });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(9)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(9)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  else{
    ques = 10;
    self.setState({
      topic:"LVE"
    });
      setTimeout(function () {
        setInterval(function(){
          if(x){
            $('#wheel div.sec:nth-child(10)').addClass('black')
          }
          else{
            $('#wheel div.sec:nth-child(10)').removeClass('black')
          }
          x=!x;
        }, 500);
    },5000);
  }
  // console.log("ques passed",x);
  self.setState({
    question:ques
  })


  $('#wheel .sec').each(function(){
    var t = $(this);
    var noY = 0;

    var c = 0;
    var n = 100;
    var interval = setInterval(function () {
      c++;
      if (c === n) {
        clearInterval(interval);
      }

      var aoY = t.offset().top;
      setTimeout(function () {

        var x = document.getElementById("myModal")
        x.classList.remove("fade");
        x.classList.add("show");
      }, 8500);
        // console.log(c,">>",aoY);

      if(aoY < 24.89){
        // console.log('<<<<<<<<');
        $('#tx').addClass('spin');
        setTimeout(function () {
          $('#spin').removeClass('spin');
        }, 100);
      }
    }, 10);

    $('#inner-wheel').css({
      'transform' : 'rotate(' + totalDegree + 'deg)'
    });

    noY = t.offset().top;

  });
 }
}
    componentDidMount(){

      try {
        const json = localStorage.getItem('coincount');
        const coincount = JSON.parse(json);

        if (coincount) {
          this.setState(() => ({ coincount }));
        }
        else{
          const json = JSON.stringify(this.state.coincount);
          localStorage.setItem('coincount', json);
        }
      } catch (e) {
        // Do nothing at all
      }
    }

  render(){
    return (
      <div id="omniApp">
      <div  className="flex " style={{background: "white" , position:"fixed",top:"0",zIndex:"10000",width:"100%"}}>
        <div  className="flex col-xs-12 first-row">
          <div  className="flex omni-container h-100">
            <div  className="flex row h-100 align-items-center header">
              <div  className="flex col-12 col-sm-3 col-md-4 col-lg-3 xs-t-b-10 logoNew">
                <div  className="flex logoWrapper">
                  <a  href="/" className="logo-link">
                    <img style={{  marginLeft: "15px",width: "50%"}} src="https://dev.omnicuris.com/img/omnicuris_logo.png" alt=""/>
                    <h6  className="logo_tagline">India's Online CME Platform</h6>
                  </a>
                </div>

              </div>
              <p className="pts">{this.state.coincount}</p>

              <img src={Coin} alt="Treasure" className="coin header-coin"/>

            </div>
          </div>
        </div>
      </div>
      <div className="App" style={{position: "relative",top: "60px"}}>
      <div className="col-xs-12 col-sm-4" id="left-head">
      </div>
      <div className="col-sm-4 col-xs-12" id="heading">
      <div className="col-xs-12 col-sm-4">
      <img  src="http://s3-ap-southeast-1.amazonaws.com/omnicuris-backend/t_organizations/images/000/000/067/original/CSI.png" alt="" className="cardio_img"/>
      </div>
      <div className="col-xs-12 col-sm-8" id="p-l-0">
      <h2>Cardiology society of India</h2>
      <p>ECG Quiz Series Basic Level 1</p>
      </div>
      </div>

      <div className="modal fade" id="myModal" role="dialog">
        <div className="modal-dialog "style={{position: "initial"}}>
          <div className="modal-content" style={{top: "400px",color: "#5f5f5f"}}>

            <div className="modal-body" style={{textAlign:"center"}}>
              <h3 >Your JackPot Question is on {this.state.topic}</h3>
              <a href={`/page2/${this.state.question}`}>
                <button type="button" className="btn-lg primaryoc" style={{width:"inherit"}}>NEXT</button>
              </a>
            </div>

          </div>
        </div>
      </div>

      <Animated animationIn="zoomInDown"  isVisible={true}>

        <div id="wrapper"  className="slideInUp">

                <div id="wheel" className="slideInUp">
                    <div id="inner-wheel" className="slideInUp">
                        <div className="sec"><span className="fa ">STEMI</span></div>
                        <div className="sec"><span className="fa ">AMI</span></div>
                        <div className="sec"><span className="fa ">RBBB</span></div>
                        <div className="sec"><span className="fa ">LAHB</span></div>
                        <div className="sec"><span className="fa ">AVNRT</span></div>
                        <div className="sec"><span className="fa ">AVRT</span></div>
                        <div className="sec"><span className="fa ">SSS</span></div>
                        <div className="sec"><span className="fa ">AV BLOCK</span></div>
                        <div className="sec"><span className="fa ">RVE</span></div>
                        <div className="sec"><span className="fa ">LVE</span></div>
                    </div>

                    <div id="shine"></div>
                </div>
                <Animated animationIn="flash" animationInDelay="1000" isVisible={true}>
                <div id="spin" onClick={this.spin}>
                    <div id="inner-spin"></div>
                </div>
                </Animated>

          </div>
      </Animated>
      </div>

      </div>
    );
  }
}

export default Wheel;
